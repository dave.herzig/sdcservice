package ch.fhnw.restservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class DataContainer {

    private static DataContainer instance;
    private final AtomicLong idgen = new AtomicLong();

    private Map<String, Long> experiments = new HashMap<>();
    private Map<Long, List<DataObject>> data = new HashMap();

    public static DataContainer getInstance() {
        if (instance == null) {
            instance = new DataContainer();
        }
        return instance;
    }

    private DataContainer() {
        // singleton
    }

    public Boolean createExperiment(String name) {
        Long identifier = experiments.get(name);
        if (identifier == null) {
            identifier = idgen.incrementAndGet();
            experiments.put(name, identifier);
            List<DataObject> dataContainer = new ArrayList<>();
            data.put(identifier, dataContainer);
            return true;
        }
        return false;
    }

    public Long getExperimentId(String name) {
        return experiments.get(name);
    }

    public void storeData(Long experimentId, DataObject dObj) throws Exception {
        // check if data object is not null
        if (dObj == null) {
            throw new Exception("provided data object is null");
        }

        // check if experiment exist
        if (!experiments.values().contains(experimentId)) {
            throw new Exception("experiment id " + experimentId + " does not exist!");
        }

        // check if experiment id matches the information in the data
        if (!experimentId.equals(dObj.getExperimentId())) {
            throw new Exception(("experiment id " + experimentId + " does not match with data experiment id " + dObj.getExperimentId()));
        }

        List<DataObject> dataObjects = data.get(experimentId);
        dObj.setId(idgen.incrementAndGet());
        dataObjects.add(dObj);
    }

    public List<DataObject> getData(Long experimentId) {
        return data.get(experimentId);
    }

    public void load() {
        // TODO
    }

    public void save() {
        // TODO
    }

}
